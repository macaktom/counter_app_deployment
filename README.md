# Knowledge test

## Requirements:
1. Ansible collection community.docker needs to be installed (community.docker)
https://galaxy.ansible.com/ui/repo/published/community/docker/?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW&version=3.8.1
[Community.docker](https://galaxy.ansible.com/ui/repo/published/community/docker/?extIdCarryOver=true&sc_cid=701f2000001OH7YAAW&version=3.8.1)
2. If firewall/security groups are used, then traffic to host port (host_app_port) needs to be allowed.


## Installation:
1. Setup Ansible >= 2.11 and install community.docker collection
2. Setup SSH connection (via keys) between control and managed node.
3. Configure inventory variables (server1.yml, vault). Vault password was sent to you in email.
4. Run playbook using this command: ansible-playbook main.yml --become --ask-vault-pass

## My test:
1. I setup two EC2 instances (Ubuntu 22.04, user ubuntu) and established SSH connection via keys.
2. Run playbook: ansible-playbook main.yml --become --ask-vault-pass
3. Result: [Counter app deployment](http://3.75.212.56/)
4. Test Redis and PostgreSQL persistence (I've torn down whole environment except volumes and then recreate whole infrastructure via Docker Compose)
5. Counter didn't count from beginning, but from last inserted value.

## Ansible deployment
Possible improvements:
- Documentation
- For PostgreSQL DB -> superuser postgres is used (better to have separate user with SELECT AND INSERT permissions for counter_app database -> this could be accomplished using Jinja2 template for init.sql, which would be later parsed and inserted into container
- Add new user for counter_app (ansible user would be used as service user, only allowed to run playbooks)
- Better error handling (block, rescue -> for example when starting counter app with docker compose)