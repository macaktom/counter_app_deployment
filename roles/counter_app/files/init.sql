CREATE DATABASE counter_app;

\c counter_app;

CREATE TABLE website_visit_timestamp (
    id SERIAL PRIMARY KEY,
    created_at TIMESTAMP NOT NULL
);